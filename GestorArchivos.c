#include <stdio.h>
#include <stdlib.h>
int main()
{
 
    FILE *fp;   /* file pointer*/
    char fName[30];
    char ch;
 
    printf("\nNombre del archivo (crear): ");
    scanf("%s",fName);

    if(fp==NULL)
    {
        printf("El archivo no se creo correctamente");
        exit(0); 
        
    }

 
    /*funcion para abrir archivo*/
    fp=fopen(fName,"w");
    /*verificar creacion*/
        printf("Archivo creado.");
    /*Escribir en el archivo*/
    
    
    printf("\nEscribe el texto dentro de tu archivo, usa enter para guardar y salir:\n");
	
    while( (ch=getchar())!='\n')
    {
        putc(ch,fp); /*Escribir caracteres*/
    }
 
    printf("\nTexto guardado correctamente.");
    fclose(fp);

    /*abrir de nuevo el archivo para leer*/
    fp=fopen(fName,"r");
    if(fp==NULL)
    {
        printf("\nNo se puede abrir el archivo");
        exit(0);
    }
 
    printf("\nEl contenido del documento es:\n");
    /*leer el texto que no se detecte*/
    while( (ch=getc(fp))!=EOF )
    {
        printf("%c",ch); /*imprimir en pantalla */
    }
 
    fclose(fp);
    return 0;
}
